/**
 * Created by diana on 1/04/18.
 */
import appService from '../app.service.js'
const defaultState = {
  results: [],
  requiredAmount: 0
}

const inBrowser = typeof window !== 'undefined'
const state = (inBrowser && window.__INITIAL_STATE__) ? window.__INITIAL_STATE__.machineModule : defaultState

const getters = {
  results: state => state.results
}

const actions = {
  getCash (context, requiredAmount) {
    return appService.getCash(requiredAmount).then(data => {
      context.commit('getCash', { requiredAmount, results: data })
    })
  }
}

const mutations = {
  getCash (state, resultAmount) {
    state.requiredAmount = resultAmount.requiredAmount
    state.results = resultAmount.results
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
