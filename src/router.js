/**
 * Created by diana on 1/04/18.
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import NotFound from './theme/NotFound.vue'
import Home from './theme/Home.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'is-active',
  scrollBehavior: (to, from, savedPosition) => ({y: 0}),
  routes: [
    {path: '/', component: Home},
    {path: '*', component: NotFound}
  ]
})

export default router
