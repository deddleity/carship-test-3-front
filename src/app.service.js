/**
 * Created by diana on 2/04/18.
 */
import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:3070'

axios.interceptors.request.use(function (config) {
  if (typeof window === 'undefined') {
    return config
  }
  return config
})

const appService = {
  getCash (requiredAmount) {
    return new Promise((resolve, reject) => {
      axios.post('/api/1/cash', {
        amount: requiredAmount
      })
        .then(response => {
          resolve(response.data)
        }).catch(response => {
          reject(response.data)
        })
    })
  }
}

export default appService
