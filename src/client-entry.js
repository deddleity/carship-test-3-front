/**
 * Created by diana on 2/04/18.
 */
import {app, router} from './app'

router.onReady(() => {
  app.$mount('#app')
})
